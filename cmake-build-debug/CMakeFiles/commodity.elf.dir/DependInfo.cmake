# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/home/mihai/work/gicu/stm/commodity/startup/startup_stm32f405xx.s" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/startup/startup_stm32f405xx.s.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "ARM_MATH_CM4"
  "ARM_MATH_MATRIX_CHECK"
  "ARM_MATH_ROUNDING"
  "STM32F405xx"
  "USE_HAL_DRIVER"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../Inc"
  "../Drivers/STM32F4xx_HAL_Driver/Inc"
  "../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy"
  "../Middlewares/Third_Party/FreeRTOS/Source/include"
  "../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"
  "../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F"
  "../Middlewares/ST/STM32_USB_Device_Library/Core/Inc"
  "../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc"
  "../Drivers/CMSIS/Device/ST/STM32F4xx/Include"
  "../Drivers/CMSIS/Include"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_cortex.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_dma_ex.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_exti.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_exti.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ex.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_flash_ramfunc.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_gpio.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c_ex.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_i2c_ex.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pcd_ex.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_pwr_ex.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_rcc_ex.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_spi.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_tim_ex.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_hal_uart.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Drivers/STM32F4xx_HAL_Driver/Src/stm32f4xx_ll_usb.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Src/usbd_cdc.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Src/usbd_cdc.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_core.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_core.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/Third_Party/FreeRTOS/Source/croutine.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/croutine.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/Third_Party/FreeRTOS/Source/event_groups.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/event_groups.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/Third_Party/FreeRTOS/Source/list.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/list.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang/heap_4.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang/heap_4.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/Third_Party/FreeRTOS/Source/queue.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/queue.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/Third_Party/FreeRTOS/Source/stream_buffer.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/stream_buffer.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/Third_Party/FreeRTOS/Source/tasks.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/tasks.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Middlewares/Third_Party/FreeRTOS/Source/timers.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/timers.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/freertos.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/freertos.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/main.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/main.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/stm32f4xx_hal_msp.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/stm32f4xx_hal_msp.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/stm32f4xx_hal_timebase_tim.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/stm32f4xx_hal_timebase_tim.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/stm32f4xx_it.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/stm32f4xx_it.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/syscalls.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/syscalls.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/system_stm32f4xx.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/system_stm32f4xx.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/usb_device.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/usb_device.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/usbd_cdc_if.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/usbd_cdc_if.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/usbd_conf.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/usbd_conf.c.obj"
  "/home/mihai/work/gicu/stm/commodity/Src/usbd_desc.c" "/home/mihai/work/gicu/stm/commodity/cmake-build-debug/CMakeFiles/commodity.elf.dir/Src/usbd_desc.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ARM_MATH_CM4"
  "ARM_MATH_MATRIX_CHECK"
  "ARM_MATH_ROUNDING"
  "STM32F405xx"
  "USE_HAL_DRIVER"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../Inc"
  "../Drivers/STM32F4xx_HAL_Driver/Inc"
  "../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy"
  "../Middlewares/Third_Party/FreeRTOS/Source/include"
  "../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"
  "../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F"
  "../Middlewares/ST/STM32_USB_Device_Library/Core/Inc"
  "../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc"
  "../Drivers/CMSIS/Device/ST/STM32F4xx/Include"
  "../Drivers/CMSIS/Include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
